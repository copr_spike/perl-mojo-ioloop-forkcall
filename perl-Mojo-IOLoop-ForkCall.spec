Name:           perl-Mojo-IOLoop-ForkCall
Version:        0.21
Release:        1%{?dist}
Summary:        (DEPRECATED) run blocking functions asynchronously by forking
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Mojo-IOLoop-ForkCall/
Source0:        https://www.cpan.org/authors/id/J/JB/JBERGER/Mojo-IOLoop-ForkCall-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(IO::Pipely)
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(Mojolicious) >= 5.08
BuildRequires:  perl(Perl::OSType)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Mojo)
BuildRequires:  perl(Mojo::IOLoop::Delay)
Requires:       perl(IO::Pipely)
Requires:       perl(Mojolicious) >= 5.08
Requires:       perl(Perl::OSType)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
DEPRECATED!

%prep
%setup -q -n Mojo-IOLoop-ForkCall-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf $RPM_BUILD_ROOT

./Build install --destdir=$RPM_BUILD_ROOT --create_packlist=0
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes ex META.json
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Fri Nov 12 2021 spike@fedoraproject.org 0.21-1
- Updated to 0.21
- Added perl-interpreter to BuildRequires to satisfy fedora's perl packaging guidelines
- Added Mojo::IOLoop::Delay to BuildRequires since as of Mojolicious 9.0, the package Mojo::IOLoop no longer provides it

* Mon Sep 09 2019 spike@fedoraproject.org 0.20-2
- Added Test::More and Test::Mojo to BuildRequires

* Mon Sep 09 2019 spike@fedoraproject.org 0.20-1
- Specfile autogenerated by cpanspec 1.78.
